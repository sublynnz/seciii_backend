package com.example.test1.repository;

import com.example.test1.entity.Functionmapping;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface FunctionMappingRepository extends JpaRepository<Functionmapping, Integer> {
    @Query(value = "select u from Functionmapping u where u.fromlib like %?1% group by u.fromlib, u.tolib")
    public Page<Functionmapping> selectBySour(String source, Pageable pageable);

    @Query(value = "select u from Functionmapping u where u.fromlib = ?1 and u.tolib = ?2")
    public Page<Functionmapping> selectMapping(String source, String target, Pageable pageable);
}
