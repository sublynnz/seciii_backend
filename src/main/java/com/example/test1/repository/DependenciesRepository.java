package com.example.test1.repository;

import com.example.test1.entity.Dependencies;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DependenciesRepository extends JpaRepository<Dependencies, Integer> {
    @Query(value = "select u from Dependencies u where u.aid = ?1")
    public Dependencies selectInfo(String artifactId);
}
