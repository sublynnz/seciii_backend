package com.example.test1.repository;
import com.example.test1.entity.Confidence;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ConfidenceRepository extends JpaRepository<Confidence,Integer> {
    @Query(value = "select u from Confidence u where u.source_lib like %?1%")
    public Page<Confidence> selectBySour(String source, Pageable pageable);

}
