package com.example.test1.repository;
import com.example.test1.entity.Project;
import com.example.test1.entity.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface RepositoryRepository extends JpaRepository<Repository,Integer>{
    @Query(value="select u from Repository u where u.repository_name like %?1% ")
    public Page<Repository> queryKeyword(String keyword, Pageable pageable);
}
