package com.example.test1.repository;

import com.example.test1.entity.It2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface It2Repository extends JpaRepository<It2,Integer> {
    @Query(value = "select u from It2 u group by u.name")
    public Page<It2> selectNameList(Pageable pageable);

    @Query(value = "select u from It2 u where u.name = ?1")
    public Page<It2> selectVersion(String name, Pageable pageable);

    @Query(value = "select u.dependency from It2 u where u.name=?1 and u.version = ?2")
    public String selectDependency(String name, String version);

    public List<It2> findByName(String name);
}
