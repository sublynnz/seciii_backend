package com.example.test1.repository;

import com.example.test1.entity.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.PathVariable;

public interface ProjectRepository extends JpaRepository<Project,Integer> {
    @Query(value="select u from Project u where u.project_name like %?1% ")
    public Page<Project> queryKeyword(String keyword, Pageable pageable);

//    @Query(value="select *")
}
