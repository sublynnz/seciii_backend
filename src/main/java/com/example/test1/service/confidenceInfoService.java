package com.example.test1.service;


import com.example.test1.entity.Confidence;
import com.example.test1.repository.ConfidenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@Service
public class confidenceInfoService {
    @Autowired
    private ConfidenceRepository confidenceRepository;

    public void saveInfo(Confidence confidence){
        try {
            // 加载数据库驱动
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://106.15.56.189:3306/seiiitest?useUnicode=true&characterEncoding=utf-8&useSSL=false";
            // 数据库用户名
            String user = "root";
            // 数据库密码
            String password = "123456";
            // 建立数据库连接，获得连接对象conn
            Connection conn = DriverManager.getConnection(url, user, password);
            String sql = "insert into confidence (source_lib,target_lib,confident,version,times) values(?,?,?,?,?)"; // 生成一条sql语句
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, confidence.getSource_lib());
            ps.setString(2, confidence.getTarget_lib());
            ps.setDouble(3, confidence.getConfident());
            ps.setString(4, confidence.getVersion());
            ps.setInt(5,confidence.getTimes());
            ps.executeUpdate();
            conn.close();
            System.out.println("插入完毕！！！");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
