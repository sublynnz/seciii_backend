package com.example.test1.service;

import com.example.test1.dao.Segment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

//为一条迁移规则计算推荐方法的算法
public class SA {
    ArrayList<Segment> segmentList;

    public SA(final ArrayList<Segment> segmentList){
        this.segmentList = new ArrayList<Segment>();
        this.segmentList.addAll(segmentList);

    }

    class ReturnIntersectedSegment {
        ArrayList<Segment> intersectedSegment = new ArrayList<Segment>();
        boolean hasIntersection;
    }

    public void procedure(){
        //segment数不得小于2
        if(segmentList.size() < 2){
            return;
        }

        boolean hasMoreIntersection = false;

        ArrayList<Segment> segmentListAll = new ArrayList<Segment>();
        segmentListAll.addAll(segmentList);

        //不能用while循环，hasMoreIntersection初始值为false，需保证循环体至少执行一次
        do{
            if (segmentListAll.size() == 1) {
                break;
            }

            //堆排序
            Collections.sort(segmentListAll);

            ArrayList<Segment> segmentListNew = new ArrayList<Segment>();

            //找两个segment之间的交集
            for (int i = 0; i < segmentListAll.size()-1; i++) {
                hasMoreIntersection = false;
                for(int j = i+1; j < segmentListAll.size(); j++){
                    ArrayList<Segment> intersectingSegments = new ArrayList<Segment>();
                    segmentListNew.clear();
                    segmentListNew.addAll(segmentListAll);

                    Segment segment1 = segmentListAll.get(i);
                    Segment segment2 = segmentListAll.get(j);

                    ReturnIntersectedSegment returnIntersectedSegment = intersectSegments(segment1, segment2);
                    if (returnIntersectedSegment.hasIntersection) {
                        // If we found intersection add ad new fragments and remove shared code between two fragments
                        hasMoreIntersection = true;
                        // intersectingSegments.addAll( intersectSegments(segment1,segment2) );
                        intersectingSegments.addAll(returnIntersectedSegment.intersectedSegment);
                        segmentListNew.remove(segment1);
                        segmentListNew.remove(segment2);
                    }

                    //对于新产生的segment，若原来存在，其frequency+1；若不存在，将其新增至segmentListNew中
                    for (Segment newSegment : intersectingSegments) {
                        boolean isAdded = false;
                        for (int k = 0; k < segmentListNew.size(); k++) {
                            // TODO: need fix for n-m mapping
                            if (segmentListNew.get(k).addedCode.equals(newSegment.addedCode)
                                    && segmentListNew.get(k).removedCode.equals(newSegment.removedCode)) {
                                segmentListNew.get(k).frequency +=  1;
                                isAdded = true;
                            }
                        }
                        if (isAdded == false) {
                            segmentListNew.add(newSegment);
                        }
                    }

                    // !!! 内层循环找到有交集的segment，处理完后要跳出内存循环，否则会导致外层循环的一个segment与多个有交集的segment比较。
                    if (hasMoreIntersection) {
                        break;
                    }
                }

                if (hasMoreIntersection) {
                    break;
                }
            }

            segmentListAll.clear();
            segmentListAll.addAll(segmentListNew);

            if(hasMoreIntersection == false) {
                //检查有没有多对多映射
                ArrayList<Segment> segmentListNeedBreak = new ArrayList<Segment>();
                int len = segmentListAll.size();
                ArrayList<Segment> segmentListnm = new ArrayList<Segment>();
                segmentListnm.addAll(segmentListAll);
                Segment maxSegment = new Segment();
                //System.out.println(segmentListAll.size());

                for (Segment segment : segmentListnm) {
                    if (segment.getTotalLinesNumbers() > 2) {
                        //segmentListNeedBreak.add(segment);
                        Segment s1 = new Segment();
                        s1.frequency = 0;
                        s1.fromLibVersion = segment.fromLibVersion;
                        s1.toLibVersion = segment.toLibVersion;
                        //使用余弦相似度判断取哪一个
                        cosSimilarity cs =new cosSimilarity();
                        String[] temp = cs.compareCos(segment.removedCode,segment.addedCode);

                        s1.removedCode.add(temp[0]);
                        s1.addedCode.add(temp[1]);

                        //判断新segment是否重复
                        boolean isAdded = false;
                        for(int m = 0; m < segmentListnm.size(); m++) {
                            if (segmentListnm.get(m).addedCode.equals(s1.addedCode)
                                    && segmentListnm.get(m).removedCode.equals(s1.removedCode)) {
                                segmentListnm.get(m).frequency +=  1;
                                isAdded = true;
                            }
                        }
                        if (isAdded == false) {
                            segmentListAll.add(s1);
                        }
                    }
                }

                if (!(len == segmentListAll.size())) {
                    // 有多对多映射,继续循环
                    hasMoreIntersection = true;
                }

            }

        }while(hasMoreIntersection);

        //插入数据库
        for (Segment segment : segmentListAll) {
            String FromCode = arrayListToString(segment.removedCode);
            String ToCode = arrayListToString(segment.addedCode);
            String fromLib = segment.fromLibVersion.split(":")[1];
            String toLib = segment.toLibVersion.split(":")[1];
            int frequency = segment.frequency;
            insertInto(FromCode, ToCode, fromLib, toLib, frequency);
        }

    }

    //寻找两个segment的交集
    ReturnIntersectedSegment intersectSegments(Segment segment1, Segment segment2) {
        ReturnIntersectedSegment returnIntersectedSegment = new ReturnIntersectedSegment();
        returnIntersectedSegment.hasIntersection = false;

        //The new segment inherite the fromLibVersion, toLibVersion from segment1 by default
        Segment segmentNew = new Segment(segment1.fromLibVersion,segment1.toLibVersion);
        Segment segment1New = new Segment(segment1.fromLibVersion,segment1.toLibVersion);
        segment1New.frequency = segment1.frequency; // Inherit the frequency
        Segment segment2New = new Segment(segment2.fromLibVersion,segment2.toLibVersion);
        segment2New.frequency = segment2.frequency;// Inherit the frequency

        for (String funName : segment1.removedCode) {
            if (segment2.removedCode.contains(funName)) {
                if (segmentNew.removedCode.contains(funName) == false) {
                    segmentNew.removedCode.add(funName);
                }
            }
        }
        for (String funName : segment1.addedCode) {
            if (segment2.addedCode.contains(funName)) {
                if (segmentNew.addedCode.contains(funName) == false) {
                    segmentNew.addedCode.add(funName);
                }
            }
        }

        // new segment 1 removedCode
        for (String funName : segment1.removedCode) {
            if (segmentNew.removedCode.contains(funName) == false) {
                segment1New.removedCode.add(funName);
            }
        }
        // new segment 1 addedCode
        for (String funName : segment1.addedCode) {
            if (segmentNew.addedCode.contains(funName) == false) {
                segment1New.addedCode.add(funName);
            }
        }

        // new segment 2 removedCode
        for (String funName : segment2.removedCode) {
            if (segmentNew.removedCode.contains(funName) == false) {
                segment2New.removedCode.add(funName);
            }
        }
        // new segment 2 addedCode
        for (String funName : segment2.addedCode) {
            if (segmentNew.addedCode.contains(funName) == false) {
                segment2New.addedCode.add(funName);
            }
        }

        if (segmentNew.addedCode.size() == 0 || segmentNew.removedCode.size() == 0) {
            return returnIntersectedSegment;
        }

        // TODO: this code may need improrve in find 1:N
        // generate new Segment 1 and 2 without Segment
        if (segmentNew.addedCode.size() > 0 && segmentNew.removedCode.size() > 0) {
            segmentNew.frequency = segment1.frequency + segment2.frequency;
            returnIntersectedSegment.intersectedSegment.add(segmentNew);
            returnIntersectedSegment.hasIntersection = true;
        }
        if (segment1New.addedCode.size() > 0 && segment1New.removedCode.size() > 0)
            returnIntersectedSegment.intersectedSegment.add(segment1New);
        if (segment2New.addedCode.size() > 0 && segment2New.removedCode.size() > 0)
            returnIntersectedSegment.intersectedSegment.add(segment2New);

        return returnIntersectedSegment;
    }

    public void insertInto (String FromCode, String ToCode, String fromLib, String toLib, int frequency) {
        try {
            Connection c;
            Class.forName("com.mysql.cj.jdbc.Driver");
            String url = "jdbc:mysql://106.15.56.189/seiiitest?useUnicode=true&characterEncoding=utf-8&useSSL=false";
            // 数据库用户名
            String user = "root";
            // 数据库密码
            String password = "123456";
            c = DriverManager.getConnection(url, user, password);

            c.setAutoCommit(false);
            // stmt = c.createStatement();
            String sql = "INSERT INTO functionmapping (FromCode,ToCode,fromLib,toLib, frequency) VALUES (?,?,?,?,?);";
            PreparedStatement stmt = c.prepareStatement(sql);
//            stmt.setInt(1, MigrationRuleID);
            stmt.setString(1, FromCode);
            stmt.setString(2, ToCode);
            stmt.setString(3, fromLib);
            stmt.setString(4, toLib);
            stmt.setInt(5,frequency);
            stmt.executeUpdate();

            // stmt.executeUpdate(sql);
            stmt.close();
            c.commit();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        System.out.println("Records created successfully");
    }

    public static String arrayListToString(ArrayList<String> listOfCode) {
        StringBuilder textCode = new StringBuilder();
        for (String lineCode : listOfCode)
            textCode.append(lineCode).append('\n');
        return textCode.toString();
    }
}
