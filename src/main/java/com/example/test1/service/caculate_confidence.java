package com.example.test1.service;

import com.example.test1.entity.Confidence;
import com.example.test1.entity.It2;
import com.example.test1.repository.It2Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class caculate_confidence {
    @Autowired
    private It2Repository it2Repository ;


    public HashSet<String> getAllProjectName(){
        HashSet<String> names = new HashSet<>();
        List<It2> it2s=it2Repository.findAll();
        for (It2 each:it2s){
            names.add(each.getName());
        }
        return names;
    }

    // 获取一个项目 所有版本两两比较的迁移规则
    public ArrayList<HashMap> getRules(List<It2> it2s) {
        dependency_caculate dc = new dependency_caculate();

        for (int i = 0; i < it2s.size(); i++) {
            if(i== it2s.size()-1){
                continue;
            }
            String depen1 = it2s.get(i).getDependency();
            String depen2 = it2s.get(i+1).getDependency();
            int len1 = depen1.split(",").length;
            int len2 = depen2.split(",").length;
            HashSet<String> h1 = new HashSet<>();
            HashSet<String> h2 = new HashSet<>();
            h1.addAll(Arrays.asList(depen1.split(",")).subList(0, len1));
            h2.addAll(Arrays.asList(depen2.split(",")).subList(0, len2));
            HashSet<String> remSet = dc.rem(h1, h2);
            HashSet<String> addSet = dc.add(h1, h2);
            dc.Transfer_rule(addSet, remSet,it2s.get(i).getVersion(),it2s.get(i+1).getVersion(),it2s.get(i).getName());
        }
        HashMap<String,Integer> M = new HashMap<String,Integer>();
        HashMap<String,String>  MV =new HashMap<String,String>();
        M =dc.get_rule();
        MV = dc.get_version();
        //System.out.println(M);
        //System.out.println(MV);
        ArrayList<HashMap> t =new ArrayList<>();
        t.add(M);
        t.add(MV);
         //将迁移规则转化为Confidence
        return t;

    }

    //所有的项目进行计算
    public  void doCaculate(){
        HashMap<String,Integer> MRes =new HashMap<>();
        HashMap<String,String> MVRes =new HashMap<>();
        HashSet<String> names = getAllProjectName();
        for(String name:names){
            List<It2> it2s = it2Repository.findByName(name);
            ArrayList<HashMap> list =new ArrayList<>();
            caculate_confidence tmp =new caculate_confidence();
            list = getRules(it2s);
            HashMap<String,Integer> t1 =new HashMap<>();
            HashMap<String,String> tv1 =new HashMap<>();
            t1 = list.get(0);
            tv1= list.get(1);
            for(Map.Entry<String, Integer> each : t1.entrySet()){
                MRes.merge(each.getKey(),each.getValue(),(oldValue, newValue) -> oldValue + newValue);
            }
            for(Map.Entry<String, String> each : tv1.entrySet()){
                MVRes.merge(each.getKey(),each.getValue(),(oldValue, newValue) -> oldValue + "," + newValue);
            }
        }
        //System.out.println(MRes);
        //System.out.println(MVRes);

        dependency_caculate res = new dependency_caculate();
        MRes = res.conf(MRes);
        //System.out.println(M);
        for(Map.Entry<String,Integer> each:MRes.entrySet()) {
            String[] temp = each.getKey().split("&");
            Double confident = Double.parseDouble(temp[1]);
            String source = temp[0].split(",")[0];
            String target = temp[0].split(",")[1];
            String version = "";
            for(Map.Entry<String,String> other:MVRes.entrySet()){
                if(other.getKey().equals(temp[0])){
                    version = other.getValue();
                }else{
                    continue;
                }
            }
            Integer times = each.getValue();
            System.out.println("source:"+source+" "+"target:"+target + " confident:"+confident +" "+" times:"+each.getValue());
            Confidence confidence = new Confidence(source, target, confident, version,times);
            confidenceInfoService cf = new confidenceInfoService();
            cf.saveInfo(confidence);
        }
    }

}
