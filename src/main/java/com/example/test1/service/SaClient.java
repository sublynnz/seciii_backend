package com.example.test1.service;

import com.example.test1.dao.Segment;
import com.example.test1.entity.Migrationsegments;

import java.sql.*;
import java.util.ArrayList;

public class SaClient {

    //不要运行这个main函数！！！！会导致数据库内容改变！！！！！
    //初始化数据库
    public static void main(String[] args) {
        // TODO Auto-generated method stub

        new SaClient().run();
    }

    public void run() {
        MigrationSegmentsDB migrationSegmentsDB = new MigrationSegmentsDB();
        ArrayList<Integer> ruleIDList = migrationSegmentsDB.getMigrationRuleIDs();
        int len = ruleIDList.size();
        for (int i = 0; i < len; i++) {
            int ruleID = ruleIDList.get(i);
            ArrayList<Segment> segmentList = migrationSegmentsDB.getSegmentsObj(ruleID);
            SA sa = new SA(segmentList);
            sa.procedure();
        }

    }








}
