package com.example.test1.service;

import com.example.test1.dao.Segment;
import com.example.test1.entity.Migrationsegments;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;

//将数据库取到的数据（MigrationSegment）存入持久化类型（Segment）中
public class MigrationSegmentsDB {


    // get segments as obj from database
    public ArrayList<Segment> getSegmentsObj(int migrationRuleID) {
        ArrayList<Segment> segmentList = new ArrayList<Segment>();

        // read from database
        LinkedList<Migrationsegments> listOfSegments = new MigrationSegmentsDB().getMigrationSegments(migrationRuleID);
        for (Migrationsegments migrationSegments : listOfSegments) {
            Segment segment = new Segment(migrationSegments);

            //为addCode和removeCode赋值
            String[] linesRemoved = migrationSegments.FromCode.split("\n");
            for (String line : linesRemoved) {
                segment.removedCode.add(line);
            }


            String[] linesAdded = migrationSegments.ToCode.split("\n");
            for (String line : linesAdded) {
                segment.addedCode.add(line);
            }
            segmentList.add(segment);
        }

        return segmentList;
    }


    //从数据库中取出Migrationsegments类型的数据
    public LinkedList<Migrationsegments> getMigrationSegments(int migrationRuleID) {
        LinkedList<Migrationsegments> listOfSegmnets = new LinkedList<Migrationsegments>();
        Statement stmt = null;
        try {
            Connection c = null;
            Class.forName("com.mysql.cj.jdbc.Driver");
            String url = "jdbc:mysql://localhost/seiiitest?useUnicode=true&characterEncoding=utf-8&useSSL=false";
            // 数据库用户名
            String user = "root";
            // 数据库密码
            String password = "123456";
            c = DriverManager.getConnection(url, user, password);

            c.setAutoCommit(false);
            stmt = c.createStatement();
            ResultSet rs = stmt
                    .executeQuery("select * from migrationsegments WHERE  MigrationRuleID=" + migrationRuleID);

            while (rs.next()) {
                listOfSegmnets.add(new Migrationsegments(rs.getInt("MigrationRuleID"), rs.getInt("AppID"), rs.getString("CommitID"),
                        rs.getString("FromCode"), rs.getString("ToCode"), rs.getString("fileName"),
                        rs.getString("fromLibVersion"), rs.getString("toLibVersion"), rs.getInt("id")));
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());

        }

        return listOfSegmnets;
    }

    //取出数据库中有迁移映射的迁移规则id（migrationRuleID）
    public ArrayList<Integer> getMigrationRuleIDs() {
        ArrayList<Integer> ruleIDList = new ArrayList<Integer>();
        Statement stmt = null;
        try {
            Connection c = null;
            Class.forName("com.mysql.cj.jdbc.Driver");
            String url = "jdbc:mysql://localhost/seiiitest?useUnicode=true&characterEncoding=utf-8&useSSL=false";
            // 数据库用户名
            String user = "root";
            // 数据库密码
            String password = "123456";
            c = DriverManager.getConnection(url, user, password);

            c.setAutoCommit(false);
            stmt = c.createStatement();
            ResultSet rs = stmt
                    .executeQuery("select distinct MigrationRuleID from migrationsegments");

            while (rs.next()) {
                ruleIDList.add(rs.getInt(1));
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());

        }
        return ruleIDList;
    }

}
