package com.example.test1.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class dependency_caculate {

//    class Transfer_Rule {
//        String from;
//        String to;
//        String fromVersion;
//        String toVersion;
//        public Transfer_Rule(String from, String to, String fromVersion, String toVersion) {
//            this.from = from;
//            this.to = to;
//            this.fromVersion = fromVersion;
//            this.toVersion = toVersion;
//        }
//    }
    private HashMap<String, Integer> Transfer_rules;
    private HashMap<String,String> Versions;
    public dependency_caculate() {
        Transfer_rules = new HashMap<String, Integer>();
        Versions = new HashMap<String, String>();
    }

    public HashSet<String> rem(HashSet<String> one, HashSet<String> two) {
        HashSet<String> a = new HashSet<String>();
        HashSet<String> b = new HashSet<String>();
        for (String each : one) {
            a.add(each);
        }
        for (String each : two) {
            b.add(each);
        }
        a.removeAll(b);
        return a;
    }

    public HashSet<String> add(HashSet<String> one, HashSet<String> two) {
        HashSet<String> a = new HashSet<String>();
        HashSet<String> b = new HashSet<String>();
        for (String each : one) {
            a.add(each);
        }
        for (String each : two) {
            b.add(each);
        }
        b.removeAll(a);
        return b;
    }

   //计算迁移规则的所有方法都放在这一个方法里了，只需要调用这个方法就可得到迁移规则的hashmap和version的hashmap并写入类的成员中
    public void Transfer_rule(HashSet<String> one, HashSet<String> two, String fromVersion, String toVersion,String projectName) {
        HashSet<String> add = new HashSet<String>();
        HashSet<String> rem = new HashSet<String>();
        add=add(one,two);
        rem=rem(one,two);
        if(add.size() == 0 || rem.size()==0){
            return;
        }
        HashSet<String> rule = new HashSet<>();

        for (String each : add) {
            for (String each2 : rem) {
                rule.add(each + "," + each2);
            }
        }
        HashMap<String, Integer> rule_count = new HashMap<>();

        for (String each : rule) {
            merge_rule(each, 1);
            merge_version(each, projectName +":"+fromVersion + " -> " + toVersion);
//            rule_count.put(each,1);
        }
//        return rule_count;

    }

    public void merge_rule(String rule, Integer count) {
        Transfer_rules.merge(rule, count, (oldValue, newValue) -> oldValue + newValue);
    }
    public void merge_version(String version, String version_name) {
        Versions.merge(version, version_name, (oldValue, newValue) -> oldValue + "," + newValue);
    }

    public HashMap<String, Integer> get_rule() {
        return Transfer_rules;
    }
    public HashMap<String,String> get_version(){
        return Versions;
    }


    //置信度计算
    public HashMap<String, Integer> conf(HashMap<String, Integer> rules) {
        HashMap<String, Integer> M_ = new HashMap<String, Integer>();
        double conf = 0;
        for (Map.Entry<String, Integer> each : rules.entrySet()) {
            if(each.getKey().equals("")){
                continue;
            }
            int num1 = 0;
            int num2 = 0;
            String[] temp;
            temp = each.getKey().split(",");
            if(temp.length != 2 || temp[0].equals("")){
                continue;
            }else{
                for (Map.Entry<String, Integer> i : rules.entrySet()) {
                    if( i.getKey().split(",").length != 2 ){
                        continue;
                    }else{
                        if (temp[0].equals(i.getKey().split(",")[0]) && !i.getKey().split(",")[1].equals("")) {
                            num1 = num1 + 1;
                        }
                        else if (temp[1].equals(i.getKey().split(",")[1]) && !i.getKey().split(",")[0].equals("")) {
                            num2 = num2 + 1;

                        }
                    }
                }
                int molecular = each.getValue();
                if ((double) molecular / num1 >= (double) molecular / num2) {
                    conf = Double.parseDouble(String.format("%.2f", (double) molecular / num2));
                    if (conf > 0.6)
                        M_.put(each.getKey() + "&" + conf, each.getValue());
                } else {
                    conf = Double.parseDouble(String.format("%.2f", (double) molecular / num1));
                    if (conf > 0.6)
                        M_.put(each.getKey() + "&" + conf, each.getValue());
                }
            }
        }
        return M_;
    }

}
