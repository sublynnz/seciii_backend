package com.example.test1.service;

import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.collections4.SetUtils;

public class cosSimilarity {

    public  String[] compareCos(ArrayList<String> a, ArrayList<String> b){
        String[] res =new String[2];
        double score =0;

        int len1 = a.size();
        int len2 = b.size();

        for(int i = 0 ; i <len1 ;i++){
            for(int j= 0 ;j <len2 ;j++){
                double tmp =jaccard(a.get(i),b.get(j));
                if (score < tmp ){
                    score = tmp;

                    res[0] = a.get(i);
                    res[1] = b.get(j);
                }
            }
        }
        return res;
    }

    public double jaccard(String a, String b){
        String[] str1= a.split("\\(");
        String[] str2= b.split("\\(");
        double res;

        // 都为空相似度为 1
        if (a == null || b == null) {
            return 0f;
        }
        Set<Integer> aChar = str1[0].replaceAll(" ","").chars().boxed().collect(Collectors.toSet());
        Set<Integer> bChar = str2[0].replaceAll(" ","").chars().boxed().collect(Collectors.toSet());
        // 交集数量
        int intersection = SetUtils.intersection(aChar, bChar).size();
        if (intersection == 0) return 0;
        // 并集数量
        int union = SetUtils.union(aChar, bChar).size();
        res = ((float) intersection) / (float)union;
        return res ;
    }
}