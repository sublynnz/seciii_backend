package com.example.test1.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class Migrationsegments {
    @Id
    public Integer id;

    public Integer MigrationRuleID;
    public Integer AppID;
    public String FromCode;
    public String ToCode;
    public String CommitID;
    public String fileName;
    public String fromLibVersion;
    public String toLibVersion;

    public Migrationsegments() {
        this.MigrationRuleID = 0;
        this.AppID = 0;
        this.CommitID = null;
        this.FromCode = null;
        this.ToCode = null;
        this.fileName = null;
        this.fromLibVersion = null;
        this.toLibVersion = null;
    }

    public Migrationsegments(int MigrationRuleID, int AppID, String CommitID, String FromCode, String ToCode, String fileName,
                             String fromLibVersion, String toLibVersion, int id) {
        this.MigrationRuleID = MigrationRuleID;
        this.AppID = AppID;
        this.CommitID = CommitID;
        this.FromCode = FromCode;
        this.ToCode = ToCode;
        this.fileName = fileName;
        this.fromLibVersion = fromLibVersion;
        this.toLibVersion = toLibVersion;
        this.id = id;
    }
}
