package com.example.test1.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class Project {
    @Id
    private Integer project_id;
    private String project_name;
    private String platform;
    private String language;
    private String description;
    private String homepage_url;
    private String created_timestamp;
    private String updated_timestamp;
    private String latest_release_publish_timestamp;
    private String latest_release_number;
    private Integer repository_id;
    private String repository_url;
    private String licenses;
}
