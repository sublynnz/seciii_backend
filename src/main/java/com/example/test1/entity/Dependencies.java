package com.example.test1.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

//pom文件内每个项目对应的依赖信息
@Entity
@Data
public class Dependencies {
    @Id
    private Integer id;

    //groupId
    private String gid;

    //artifactId
    private String aid;

    public Dependencies(Integer id,String gid,String aid){
        this.id = id;
        this.aid = aid;
        this.gid = gid;
    };

    public Dependencies() {

    }

}
