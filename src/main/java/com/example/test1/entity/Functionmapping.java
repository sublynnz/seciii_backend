package com.example.test1.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data

//最终方法映射结果的数据结构
public class Functionmapping {
    @Id
    public Integer id;

    public String fromcode;
    public String tocode;
    public String fromlib;
    public String tolib;
    public int frequency;

    public Functionmapping(){
        this.fromcode = null;
        this.tocode = null;
        this.fromlib = null;
        this.tolib = null;
        this.frequency = 0;
    }

    public Functionmapping(String fromcode, String tocode, String fromlib, String tolib, int frequency) {
        this.fromcode = fromcode;
        this.tocode = tocode;
        this.fromlib = fromlib;
        this.tolib = tolib;
        this.frequency = frequency;
    }
}
