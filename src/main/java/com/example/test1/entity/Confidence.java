package com.example.test1.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
@Data
public class Confidence {
    @Id
    private Integer id;
    private String source_lib;
    private String target_lib;
    private Double confident;
    private String version;
    private Integer times;


    public Confidence(String source_lib, String target_lib , Double confident , String version,Integer times){
        this.source_lib = source_lib;
        this.target_lib = target_lib;
        this.confident = confident;
        this. version = version;
        this.times =times;

    }

    public Confidence() {
    }
}
