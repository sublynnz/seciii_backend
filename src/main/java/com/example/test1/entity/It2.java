package com.example.test1.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

//迭代二 project实体类
@Entity
@Data
public class It2 {
    @Id
    private Integer id;
    private String name;
    private String version;
    private String dependency;

    public It2(Integer id, String name, String version, String dependency) {
        this.id = id;
        this.name = name;
        this.version = version;
        this.dependency = dependency;
    }

    public It2() {
    }
}
