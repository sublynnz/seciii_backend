package com.example.test1.entity;

import lombok.Data;


import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Data

public class Repository {
    @Id
    private Integer repository_id;
    private String host_type;
    private String repository_name;
    private String language;
    private String repository_owner;
    private String description;
    private String homepage_url;
    private Date created_timestamp;
    private Date updated_timestamp;
    private Date last_push_timestamp;
    private String fork;
    private Integer forks_count;
    private Integer watchers_count;
    private Integer stars_count;
    private Integer contributors_count;
    private Integer open_issues_count;
    private String licenses;
    private String default_branch;
}

