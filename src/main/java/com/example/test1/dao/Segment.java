package com.example.test1.dao;

import com.example.test1.entity.Migrationsegments;

import java.util.ArrayList;


public class Segment implements Comparable<Segment> {

    public int frequency; // home many times we did see this fragment
    public double sDegree; // how similar two code are
    public ArrayList<String> addedCode;
    public ArrayList<String> removedCode;
    public int isVaildMapping; // if the mapping is valid or not (0 or 1)
    public String fromLibVersion;
    public String toLibVersion;
    public int MigrationRuleID ;
    public int AppID;
    public String CommitID;
    public Segment() {

        this.frequency = 1;
        this.addedCode = new ArrayList<String>();
        this.removedCode = new ArrayList<String>();
    }

    public Segment(String removedCode, String addedCode, double sDegree) {

        this.addedCode = new ArrayList<String>();
        this.removedCode = new ArrayList<String>();
        this.sDegree = sDegree;
        this.addedCode.add(addedCode);
        this.removedCode.add(removedCode);
    }

    //TODO 和数据库实体类对接，用数据库实体类初始化segment
    public Segment(Migrationsegments migrationSegments) {

        this.MigrationRuleID = migrationSegments.MigrationRuleID;
        this.frequency = 1;
        this.addedCode = new ArrayList<String>();
        this.removedCode = new ArrayList<String>();
        this.AppID = migrationSegments.AppID;
        this.CommitID = migrationSegments.CommitID;
        this.fromLibVersion = migrationSegments.fromLibVersion;
        this.toLibVersion = migrationSegments.toLibVersion;
    }

    public Segment(String fromLibVersion, String toLibVersion) {

        this.frequency = 1;
        this.addedCode = new ArrayList<String>();
        this.removedCode = new ArrayList<String>();
        this.fromLibVersion =  fromLibVersion;
        this.toLibVersion = toLibVersion;
    }


    // there different between number of lines in 'addedCode' and  'countAddLines'
//because 'countAddLines' has only new it did not calculate the  appending line
//same thing for 'removedCode' and 'countRemovedLines'
    public Segment(ArrayList<String> blockCode, ArrayList<String> addedCode, ArrayList<String> removedCode) {

        this.removedCode = removedCode;
        this.addedCode = addedCode;
        this.frequency = 1;

    }


    public int getCountAddLines() {
        return this.addedCode.size();
    }

    public int getCountRemovedLines() {
        return this.removedCode.size();
    }

    public int getTotalLinesNumbers() {
        return this.addedCode.size() * this.removedCode.size();
    }


    public int compareTo(Segment o) {
        int segmentSize = this.getTotalLinesNumbers() - o.getTotalLinesNumbers();
        // if they have same number of code then order by freqnecy
        // the high freqency go first
        if (segmentSize == 0) {
            segmentSize = o.frequency - this.frequency;
        }
        return segmentSize;
    }
}

