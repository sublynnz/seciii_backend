package com.example.test1.controller;

import com.example.test1.entity.It2;
import com.example.test1.entity.Project;
import com.example.test1.repository.It2Repository;
import com.example.test1.service.dependency_caculate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

@RestController
@RequestMapping("/it2")
public class It2Handler {
    @Autowired
    private It2Repository it2Repository;

    @GetMapping("/findAll/{page}/{size}")
    public Page<It2> findAllProjects(@PathVariable("page") Integer page, @PathVariable("size") Integer size){
        Pageable pageable = PageRequest.of(page-1,size);
        return it2Repository.findAll(pageable);
    }

    @GetMapping("/findNameList/{page}/{size}")
    public Page<It2> findNameList(@PathVariable("page") Integer page, @PathVariable("size") Integer size){
        Pageable pageable = PageRequest.of(page-1,size);
        return it2Repository.selectNameList(pageable);
    }

    @GetMapping("/findVersion/{name}/{page}/{size}")
    public Page<It2> findVersion(@PathVariable("name") String name, @PathVariable("page") Integer page, @PathVariable("size") Integer size){
        Pageable pageable = PageRequest.of(page-1,size);
        return it2Repository.selectVersion(name,pageable);
    }

    @GetMapping("/calDiff/{name}/{v1}/{v2}")
    public ArrayList<HashSet<String>> calDiff(@PathVariable("name") String name, @PathVariable("v1") String v1, @PathVariable("v2") String v2){
        String depen1 = it2Repository.selectDependency(name, v1);
        String depen2 = it2Repository.selectDependency(name, v2);
        int len1 = depen1.split(",").length;
        int len2 = depen2.split(",").length;
        HashSet<String> h1 = new HashSet<>();
        HashSet<String> h2 = new HashSet<>();
        h1.addAll(Arrays.asList(depen1.split(",")).subList(0, len1));
        h2.addAll(Arrays.asList(depen2.split(",")).subList(0, len2));
        HashSet<String> remSet = new HashSet<>();
        HashSet<String> addSet = new HashSet<>();
        dependency_caculate dc = new dependency_caculate();
        remSet = dc.rem(h1,h2);
        addSet = dc.add(h1,h2);
        ArrayList<HashSet<String>> diff = new ArrayList<>();
        diff.add(remSet);
        diff.add(addSet);
        return diff;
    }



}
