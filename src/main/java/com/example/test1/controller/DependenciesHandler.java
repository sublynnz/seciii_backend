package com.example.test1.controller;

import com.example.test1.entity.Dependencies;
import com.example.test1.repository.DependenciesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequestMapping("/dependencies")
public class DependenciesHandler {
    @Autowired
    private DependenciesRepository dependenciesRepository;

    @GetMapping("findInfo/{sourceName}/{targetName}")
    public ArrayList<Dependencies> findInfo(@PathVariable("sourceName") String sourceName, @PathVariable("targetName") String targetName){
        Dependencies source = dependenciesRepository.selectInfo(sourceName);
        Dependencies target = dependenciesRepository.selectInfo(targetName);
        ArrayList<Dependencies> result = new ArrayList<>();
        result.add(source);
        result.add(target);
        return result;
    }


}
