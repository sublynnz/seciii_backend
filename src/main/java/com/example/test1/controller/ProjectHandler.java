package com.example.test1.controller;

import com.example.test1.entity.Project;
import com.example.test1.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/project")
public class ProjectHandler {
    @Autowired
    private ProjectRepository projectRepository;

    @GetMapping("/findAll/{page}/{size}")
    public Page<Project> findAllProjects(@PathVariable("page") Integer page, @PathVariable("size") Integer size){
        Pageable pageable = PageRequest.of(page-1,size);
        return projectRepository.findAll(pageable);
    }

    @GetMapping("/findById/{id}")
    public Project findProjectById(@PathVariable("id") Integer id){
        Optional<Project> optional = projectRepository.findById(id);
        if(optional !=null && optional.isPresent()){
            return optional.get();
        }
        return null;
    }


    @GetMapping("findByKeywords/{keywords}/{page}/{size}")
    public Page<Project> findProjectByKeywords(@PathVariable("keywords") String keywords, @PathVariable("page") Integer page, @PathVariable("size") Integer size){
        Pageable pageable = PageRequest.of(page-1,size);
        return projectRepository.queryKeyword(keywords, pageable);
    }
}
