package com.example.test1.controller;

import com.example.test1.entity.Confidence;
import com.example.test1.entity.Project;
import com.example.test1.repository.ConfidenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/confidence")
public class ConfidenceHandler {
    @Autowired
    private ConfidenceRepository confidenceRepository;

    @GetMapping("/findAll/{page}/{size}")
    public Page<Confidence> findAllConfidence(@PathVariable("page") Integer page, @PathVariable("size") Integer size){
        Pageable pageable = PageRequest.of(page-1,size);
        return confidenceRepository.findAll(pageable);
    }

    @GetMapping("/findBySource/{source}/{page}/{size}")
    public Page<Confidence> findBySource(@PathVariable("source") String source, @PathVariable("page") Integer page, @PathVariable("size") Integer size){
        Pageable pageable = PageRequest.of(page-1,size);
        return confidenceRepository.selectBySour(source,pageable);
    }


}
