package com.example.test1.controller;



import com.example.test1.entity.Project;
import com.example.test1.entity.Repository;
import com.example.test1.repository.RepositoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/repository")
public class RepositoryHandler {
    @Autowired
    private RepositoryRepository re;

    @GetMapping("/findAll/{page}/{size}")
    public Page<Repository> findAllRepository(@PathVariable("page") Integer page, @PathVariable("size") Integer size){
        Pageable pageable = PageRequest.of(page-1,size);
        return re.findAll(pageable);
    }

    @GetMapping("/findById/{id}")
    public Repository findRepositoryById(@PathVariable("id") Integer id){
        Optional<Repository> optional = re.findById(id);
        if(optional !=null && optional.isPresent()){
            return optional.get();
        }
        return null;
    }


    @GetMapping("findByKeywords/{keywords}/{page}/{size}")
    public Page<Repository> findRepositoryByKeywords(@PathVariable("keywords") String keywords, @PathVariable("page") Integer page, @PathVariable("size") Integer size){
        Pageable pageable = PageRequest.of(page-1,size);
        return re.queryKeyword(keywords, pageable);
    }
}

