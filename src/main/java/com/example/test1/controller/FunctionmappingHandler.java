package com.example.test1.controller;

import com.example.test1.entity.Functionmapping;
import com.example.test1.repository.FunctionMappingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/functionmapping")
public class FunctionmappingHandler {
    @Autowired
    private FunctionMappingRepository functionMappingRepository;

    @GetMapping("/findAll")
    public List<Functionmapping> findAllFunctionMappings() {
        return functionMappingRepository.findAll();
    }

    @GetMapping("/selectRulesBySource/{source}/{page}/{size}")
    public Page<Functionmapping> selectRulesBySource(@PathVariable("source") String source, @PathVariable("page") Integer page, @PathVariable("size") Integer size) {
        Pageable pageable = PageRequest.of(page-1,size);
        return functionMappingRepository.selectBySour(source, pageable);
    }

    @GetMapping("/selectMapping/{source}/{target}/{page}/{size}")
    public Page<Functionmapping> selectMapping(@PathVariable("source") String source, @PathVariable("target") String target, @PathVariable("page") Integer page, @PathVariable("size") Integer size) {
        Pageable pageable = PageRequest.of(page-1,size);
        return functionMappingRepository.selectMapping(source, target, pageable);
    }

}
