package com.example.test1.service;

import com.example.test1.entity.Migrationsegments;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;

class MigrationSegmentsDBTest {

    @Test
    void getSegmentsObj() {
    }

    @Test
    void getMigrationSegments() {
        Statement stmt = null;
        try {
            Connection c = null;
            Class.forName("com.mysql.cj.jdbc.Driver");
            String url = "jdbc:mysql://localhost/seiiitest?useUnicode=true&characterEncoding=utf-8&useSSL=false";
            // 数据库用户名
            String user = "root";
            // 数据库密码
            String password = "123456";
            c = DriverManager.getConnection(url, user, password);

            c.setAutoCommit(false);
            stmt = c.createStatement();
            ResultSet rs = stmt
                    .executeQuery("select * from migrationsegments WHERE  MigrationRuleID=" + 367);

            while (rs.next()) {
                String fromCode = rs.getString(4);
                String toCode = rs.getString(5);
                String fromLibVersion =rs.getString(6);
                String toLibVersion =rs.getString(7);
                System.out.println(fromCode+"--->"+toCode);
                System.out.println(fromLibVersion+"--->"+toLibVersion);
                System.out.println();

            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());

        }
    }

    @Test
    public void getMigrationRuleIDs() {
        Statement stmt = null;
        try {
            Connection c = null;
            Class.forName("com.mysql.cj.jdbc.Driver");
            String url = "jdbc:mysql://localhost/seiiitest?useUnicode=true&characterEncoding=utf-8&useSSL=false";
            // 数据库用户名
            String user = "root";
            // 数据库密码
            String password = "123456";
            c = DriverManager.getConnection(url, user, password);

            c.setAutoCommit(false);
            stmt = c.createStatement();
            ResultSet rs = stmt
                    .executeQuery("select distinct MigrationRuleID from migrationsegments");

            while (rs.next()) {
                int ruleID = rs.getInt(1);
                System.out.println(ruleID);
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());

        }
    }
}