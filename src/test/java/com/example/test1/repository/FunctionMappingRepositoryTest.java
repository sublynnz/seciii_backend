package com.example.test1.repository;

import com.example.test1.entity.Functionmapping;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class FunctionMappingRepositoryTest {

    @Autowired
    private FunctionMappingRepository functionMappingRepository;

   // @Test
//    void selectBySour() {
//        Pageable pageable = PageRequest.of(0,5);
//        List<Functionmapping> flist = functionMappingRepository.selectBySour("t",pageable).getContent();
//        String fromCode = flist.get(0).fromcode;
//        //System.out.println(fromCode);
//        String fc2 = "public void assertEquals(java.util.Map<?, ?>, java.util.Map<?, ?>);\n";
//        assertEquals(fromCode, fc2);
//    }

    @Test
    void selectMapping() {
        Pageable pageable = PageRequest.of(0,5);
        List<Functionmapping> flist = functionMappingRepository.selectMapping("c3p0", "HikariCP", pageable).getContent();
        String fromCode = flist.get(0).fromcode;
        String fc2 = "public abstract void setUser(java.lang.String) throws javax.naming.NamingException;\n";
        assertEquals(fromCode,fc2);
    }
}