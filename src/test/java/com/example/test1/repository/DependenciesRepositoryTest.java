package com.example.test1.repository;

import com.example.test1.entity.Dependencies;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class DependenciesRepositoryTest {
    @Autowired
    private DependenciesRepository dependenciesRepository;

    @Test
    void selectInfo() {
        Dependencies dependencies = dependenciesRepository.selectInfo("ezmorph");
        System.out.println(dependencies);
        Dependencies dependencies2 = new Dependencies(116309,"net.sf.ezmorph","ezmorph");
        assertEquals(dependencies,dependencies2);
    }
}