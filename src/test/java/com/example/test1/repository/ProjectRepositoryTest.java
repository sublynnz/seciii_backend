package com.example.test1.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ProjectRepositoryTest {

    @Autowired
    private ProjectRepository projectRepository;

    @Test
    void findAll(){
        Pageable pageable = PageRequest.of(1,5);
        System.out.println(projectRepository.findAll(pageable));
    }

    @Test
    void findById(){
        System.out.println(projectRepository.findById(31620));
    }

    @Test
    void findByKeywords(){
        Pageable pageable = PageRequest.of(1,5);
        System.out.println(projectRepository.queryKeyword("ad",pageable));
    }
}