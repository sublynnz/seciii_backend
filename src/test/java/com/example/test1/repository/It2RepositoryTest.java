package com.example.test1.repository;

import com.example.test1.service.dependency_caculate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class It2RepositoryTest {
    @Autowired
    private It2Repository it2Repository;

    @Test
    void selectNameList(){
        Pageable pageable = PageRequest.of(1,5);
        System.out.println(it2Repository.selectNameList(pageable));
    }

    @Test
    void selectVersion() {
        Pageable pageable = PageRequest.of(1,5);
        System.out.println(it2Repository.selectVersion("DialogHelper",pageable));

    }

    @Test
    void selectDependency(){
        String depen = it2Repository.selectDependency("scala-io-core_2.9.1","0.4.0");
        System.out.println(depen);
        assertEquals(depen,"scala-library,junit-interface,scala-arm_2.9.1,akka-actor");
        int len = depen.split(",").length;
        //System.out.println(len);
        HashSet<String> h1 = new HashSet<>();
        h1.addAll(Arrays.asList(depen.split(",")).subList(0, len));
        System.out.println(h1);
    }

    @Test
    void diff(){
        dependency_caculate dc = new dependency_caculate();

        HashSet<String> b1 = new HashSet<>();
        HashSet<String> b2 = new HashSet<>();
        b1.add("a");
        b2.add("a");
        b2.add("b");
        b2.add("c");
        b2.add("d");

        HashSet<String> rem = new HashSet<>();
        HashSet<String> add = new HashSet<>();
        rem = dc.rem(b1,b2);
        add=dc.add(b1,b2);
        System.out.println(rem);
        System.out.println(add);
        HashSet<String> rem2 = new HashSet<>();
        HashSet<String> add2 = new HashSet<>();
        add2.add("b");
        add2.add("c");
        add2.add("d");
        assertEquals(rem,rem2);
        assertEquals(add,add2);
    }


}