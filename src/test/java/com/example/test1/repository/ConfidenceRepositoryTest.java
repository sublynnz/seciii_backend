package com.example.test1.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ConfidenceRepositoryTest {
    @Autowired
    private ConfidenceRepository confidenceRepository;

    @Test
    void selectBySour() {
        Pageable pageable = PageRequest.of(1,5);
        System.out.println(confidenceRepository.selectBySour("4",pageable));
    }
}