package com.example.test1.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class RepositoryRepositoryTest {

    @Autowired
    private RepositoryRepository repositoryRepository;

    @Test
    void findAll(){
        Pageable pageable = PageRequest.of(1,5);
        System.out.println(repositoryRepository.findAll(pageable));
    }

    @Test
    void findById(){
        System.out.println(repositoryRepository.findById(1051));
    }

    @Test
    void findByKeywords(){
        Pageable pageable = PageRequest.of(1,5);
        System.out.println(repositoryRepository.queryKeyword("a",pageable));
    }
}