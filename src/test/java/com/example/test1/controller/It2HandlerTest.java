package com.example.test1.controller;

import com.example.test1.repository.It2Repository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = It2Handler.class)
@EnableWebMvc
@AutoConfigureMockMvc
class It2HandlerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    @MockBean
    private It2Repository it2Repository;

    @Test
    void findAllProjects() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/it2/findAll/1/5"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void findNameList() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/it2/findNameList/1/5"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void findVersion() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/it2/findVersion/sapient/1/5"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }
}