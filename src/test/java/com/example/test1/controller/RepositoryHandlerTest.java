package com.example.test1.controller;

import com.example.test1.repository.RepositoryRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = RepositoryHandler.class)
@EnableWebMvc
@AutoConfigureMockMvc
class RepositoryHandlerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    @MockBean
    private RepositoryRepository re;

    @Test
    void findAllRepository() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/repository/findAll/1/5"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void findRepositoryById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/repository/findById/1051"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void findRepositoryByKeywords() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/repository/findByKeywords/a/1/5"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }
}