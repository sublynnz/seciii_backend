package com.example.test1.controller;

import com.example.test1.repository.FunctionMappingRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = FunctionmappingHandler.class)
@EnableWebMvc
@AutoConfigureMockMvc
class FunctionmappingHandlerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    @MockBean
    private FunctionMappingRepository functionMappingRepository;

    @Test
    void findAllFunctionMappings() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/functionmapping/findAll"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void selectRulesBySource() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/functionmapping/selectRulesBySource/t/1/5"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void selectMapping() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/functionmapping/selectMapping/easymock/mockito-core/1/5"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }
}