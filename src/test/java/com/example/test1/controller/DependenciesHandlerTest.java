package com.example.test1.controller;

import com.example.test1.repository.ConfidenceRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = DependenciesHandler.class)
@EnableWebMvc
@AutoConfigureMockMvc
class DependenciesHandlerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    @MockBean
    private DependenciesHandler dependenciesHandler;

    @Test
    void findInfo() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/dependencies/findInfo/org.openimaj/com.github.sbocq"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }
}