package com.example.test1.controller;

import com.example.test1.repository.ConfidenceRepository;
import com.example.test1.repository.It2Repository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = ConfidenceHandler.class)
@EnableWebMvc
@AutoConfigureMockMvc
class ConfidenceHandlerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    @MockBean
    private ConfidenceRepository confidenceRepository;

    @Test
    void findAllConfidence() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/confidence/findAll/1/5"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void findBySource() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/confidence/findBySource/5/1/5"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }
}