package com.example.test1.controller;

import com.example.test1.entity.Project;
import com.example.test1.repository.ProjectRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = ProjectHandler.class)
@EnableWebMvc
@AutoConfigureMockMvc
class ProjectHandlerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    @MockBean
    private ProjectRepository projectRepository;

    @Test
    void findAllProjects() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/project/findAll/1/5"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void findProjectById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/project/findById/31620"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void findProjectByKeywords() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/project/findByKeywords/ad/1/5"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }
}